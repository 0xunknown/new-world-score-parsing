"""
Copyright 2022 0xunknown <djbey@protonmail.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import os
import sys

def save_output_to_csv(results_dir: str, team_data: dict) -> None:
    """Save output based ont Terrbl format
        Params:
            results_dir   (str): directory output
            overall_data (dict): parsed player stats

            "name"  : name,
            "score" : score_text[0],
            "kills" : score_text[1],
            "deaths": score_text[2],
            "assist": score_text[3],
            "heals" : "err" if len(score_text) < 6 else score_text[4],
            "damage": "err" if len(score_text) < 6 else score_text[5],
            "group" : group_numbers[self._group_name_alignment(group_numbers, name)],
            "team"  : team
    """

    team_1 = {
        "groups": {}, "total_dmg": 0, "total_heals": 0,
        "total_deaths": 0, "non-healers": 0, "healers": 0,
        "avg_heals_per_healer": 0, "avg_dmg": 0, "avg_kda": 0}

    team_2 = {
        "groups": {}, "total_dmg": 0, "total_heals": 0,
        "total_deaths": 0, "non-healers": 0, "healers": 0,
        "avg_heals_per_healer": 0, "avg_dmg": 0, "avg_kda": 0}

    # TEAM 1
    for name in team_data['team_1']:
        if name == "rgb_sum":
            continue
        team_1['total_dmg']    += team_data['team_1'][name]['damage']
        team_1['total_heals']  += team_data['team_1'][name]['heals']
        team_1['total_deaths'] += team_data['team_1'][name]['deaths']
        team_1['avg_kda']      += team_data['team_1'][name]['kills']
        team_1['avg_kda']      += team_data['team_1'][name]['assist']

        # TODO: Change to be > 3*std ~95% quartile. Probably use IQR
        if team_data['team_1'][name]['heals'] > 100000:
            team_1['healers'] += 1
        else:
            team_1['non-healers'] += 1

        group = str(team_data['team_1'][name]['group'])
        if group not in team_1['groups']:
            team_1['groups'][group] = {"totals": {}, "players": []}
            team_1['groups'][group]["totals"] = {
                "total_kp"    : team_data['team_1'][name]['damage'] + team_data['team_1'][name]['assist'],
                "total_dmg"   : team_data['team_1'][name]['damage'],
                "total_heals" : team_data['team_1'][name]['heals'],
                "total_deaths": team_data['team_1'][name]['deaths'],
                "total_score" : team_data['team_1'][name]['score']}
        else:
            team_1['groups'][group]["totals"]["total_dmg"]    += team_data['team_1'][name]['damage']
            team_1['groups'][group]["totals"]["total_heals"]  += team_data['team_1'][name]['heals']
            team_1['groups'][group]["totals"]["total_deaths"] += team_data['team_1'][name]['deaths']
            team_1['groups'][group]["totals"]["total_score"]  += team_data['team_1'][name]['score']
            team_1['groups'][group]["totals"]['total_kp']      += team_data['team_1'][name]['damage'] + team_data['team_1'][name]['assist']

        team_1['groups'][group]["players"].append({
            "name"  : name,
            "kp"    : team_data['team_1'][name]['damage'] + team_data['team_1'][name]['assist'],
            "dmg"   : team_data['team_1'][name]['damage'],
            "heals" : team_data['team_1'][name]['heals'],
            "deaths": team_data['team_1'][name]['deaths'],
            "score" : team_data['team_1'][name]['score']})

    team_1['avg_heals_per_healer'] = team_1['total_heals']/team_1['healers']
    team_1['avg_dmg']              = team_1['total_dmg']/team_1['non-healers']

    # TODO: Should be adjusted..can be >50 for dc
    team_1['avg_kda'] = team_1['avg_kda']/50

    # TEAM 2
    for name in team_data['team_2']:
        if name == "rgb_sum":
            continue
        team_2['total_dmg']    += team_data['team_2'][name]['damage']
        team_2['total_heals']  += team_data['team_2'][name]['heals']
        team_2['total_deaths'] += team_data['team_2'][name]['deaths']
        team_2['avg_kda']      += team_data['team_2'][name]['kills']
        team_2['avg_kda']      += team_data['team_2'][name]['assist']

        # TODO: Change to be > 3*std ~95% quartile. Probably use IQR
        if team_data['team_2'][name]['heals'] > 25000:
            team_2['healers'] += 1
        else:
            team_2['non-healers'] += 1

        group = str(team_data['team_2'][name]['group'])
        if group not in team_2['groups']:
            team_2['groups'][group] = {"totals": {}, "players": []}
            team_2['groups'][group]["totals"] = {
                "total_kp"    : team_data['team_2'][name]['damage'] + team_data['team_2'][name]['assist'],
                "total_dmg"   : team_data['team_2'][name]['damage'],
                "total_heals" : team_data['team_2'][name]['heals'],
                "total_deaths": team_data['team_2'][name]['deaths'],
                "total_score" : team_data['team_2'][name]['score']}
        else:
            team_2['groups'][group]["totals"]["total_dmg"]    += team_data['team_2'][name]['damage']
            team_2['groups'][group]["totals"]["total_heals"]  += team_data['team_2'][name]['heals']
            team_2['groups'][group]["totals"]["total_deaths"] += team_data['team_2'][name]['deaths']
            team_2['groups'][group]["totals"]["total_score"]  += team_data['team_2'][name]['score']
            team_2['groups'][group]["totals"]["total_kp"]     += team_data['team_2'][name]['damage'] + team_data['team_2'][name]['assist']

        team_2['groups'][group]["players"].append({
            "name"  : name,
            "kp"    : team_data['team_2'][name]['damage'] + team_data['team_2'][name]['assist'],
            "dmg"   : team_data['team_2'][name]['damage'],
            "heals" : team_data['team_2'][name]['heals'],
            "deaths": team_data['team_2'][name]['deaths'],
            "score" : team_data['team_2'][name]['score']})

    team_2['avg_heals_per_healer'] = team_2['total_heals']/team_2['healers']
    team_2['avg_dmg']              = team_2['total_dmg']/team_2['non-healers']

    # TODO: Should be adjusted..can be >50 for dc
    team_2['avg_kda'] = team_2['avg_kda']/50

    # Write data to CSV
    with open(os.path.join(results_dir, "parsed_results.csv"), 'w+') as f:
        f.write("TEAM <1>")
        headers = [
            "Total Damage",
            "Total Healing",
            "Total Deaths",
            "Non-Healers",
            "Healers",
            "Average Heals per Healer",
            "Average Damage per Non Healer",
            "Average KP"]
        f.write(",".join(headers)+"\n")
        values = [
            team_1['total_dmg'],
            team_1['total_heals'],
            team_1['total_deaths'],
            team_1['non-healers'],
            team_1['healers'],
            team_1['avg_heals_per_healer'],
            team_1['avg_dmg'],
            team_1['avg_kda']
        ]
        values = [str(x) for x in values]
        f.write(",".join(values)+"\n")

        # Write groups
        # G1-5 headers
        headers = []
        for i in range(1,6):
            headers += [
                "Group " + str(i),
                "KP%",
                "Deaths",
                "Dmg",
                "Heals",
                "Score"
            ]
        f.write(",".join(headers) + "\n")

        total_group_values = []
        for i in range(0,5):
            group_i_values = []
            for j in range(1,6):
                if len(team_1['groups'][str(j)]["players"]) < 5:
                    # Select a team member from the empty group
                    if len(team_1['groups']['']['players']) > 0:
                        team_1['groups'][str(j)]["players"].append(team_1['groups']['']['players'][-1])
                        del team_1['groups']['']['players'][-1]
                    elif j == 5:
                        continue

                group_i_values += [
                    team_1['groups'][str(j)]["players"][i]['name'],
                    team_1['groups'][str(j)]["players"][i]['kp']/50,
                    team_1['groups'][str(j)]["players"][i]['deaths'],
                    team_1['groups'][str(j)]["players"][i]['dmg'],
                    team_1['groups'][str(j)]["players"][i]['heals'],
                    team_1['groups'][str(j)]["players"][i]['score']
                ]


            group_i_values = [str(x) for x in group_i_values]
            f.write(",".join(group_i_values) + "\n")

            total_group_values += [
                "Totals: ",
                team_1['groups'][str(i+1)]["totals"]['total_kp']/50,
                team_1['groups'][str(i+1)]["totals"]['total_deaths'],
                team_1['groups'][str(i+1)]["totals"]['total_dmg'],
                team_1['groups'][str(i+1)]["totals"]['total_heals'],
                team_1['groups'][str(i+1)]["totals"]['total_score']
            ]
        total_group_values = [str(x) for x in total_group_values]
        f.write(",".join(total_group_values) + "\n")

        f.write("\n")
        # G6-10 headers
        headers = []
        for i in range(6,11):
            headers += [
                "Group " + str(i),
                "KP%",
                "Deaths",
                "Dmg",
                "Heals",
                "Score"
            ]
        f.write(",".join(headers) + "\n")

        total_group_values = []
        for i in range(0,5):
            group_i_values = []

            for j in range(6,11):
                if len(team_1['groups'][str(j)]["players"]) < 5:
                    # Select a team member from the empty group
                    if len(team_1['groups']['']['players']) > 0:
                        team_1['groups'][str(j)]["players"].append(team_1['groups']['']['players'][-1])
                        del team_1['groups']['']['players'][-1]
                    elif j == 10:
                        continue
                group_i_values += [
                    team_1['groups'][str(j)]["players"][i]['name'],
                    team_1['groups'][str(j)]["players"][i]['kp']/50,
                    team_1['groups'][str(j)]["players"][i]['deaths'],
                    team_1['groups'][str(j)]["players"][i]['dmg'],
                    team_1['groups'][str(j)]["players"][i]['heals'],
                    team_1['groups'][str(j)]["players"][i]['score']
                ]
            group_i_values = [str(x) for x in group_i_values]
            f.write(",".join(group_i_values) + "\n")
            total_group_values += [
                "Totals: ",
                team_1['groups'][str(i+5)]["totals"]['total_kp']/50,
                team_1['groups'][str(i+5)]["totals"]['total_deaths'],
                team_1['groups'][str(i+5)]["totals"]['total_dmg'],
                team_1['groups'][str(i+5)]["totals"]['total_heals'],
                team_1['groups'][str(i+5)]["totals"]['total_score']
            ]
        total_group_values = [str(x) for x in total_group_values]
        f.write(",".join(total_group_values) + "\n")
