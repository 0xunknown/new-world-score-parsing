"""
Copyright 2022 0xunknown <djbey@protonmail.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from cmath import inf
import os
import cv2
from PIL import Image
import pytesseract as tess
import numpy as np
from pytesseract import Output

from ImagePreProcessing import ImagePreProcessing

class ScoreParsing():
    """
    """
    def __init__(self, data_dir: str) -> None:
        """
            Input/Output directories and OCR settings

            #NOTE: Cange self.image_x_offsets if you are
            using images different than what the test dir has
        """
        self.results_dir       = os.path.join("warimages", data_dir, "results")
        if(not os.path.isdir(self.results_dir)):
            os.mkdir(self.results_dir)


        self.image_dir         = os.path.join("warimages", data_dir)
        self.path_to_tesseract = r"Tesseract-OCR\\tesseract.exe"

        tess.pytesseract.tesseract_cmd = self.path_to_tesseract

        # List of images
        self.image_list = os.listdir(self.image_dir)

        # Data for CSV
        self.overall_data  = {}
        self.team_data     = {} # By team/group
        self.rgb_sum_groups = [] # Sum of rgb values
        self.last_name_key = "" # Overall data key
        self.player_count  = 0

        # Expected image row height
        self.y1 = 55
        self.y2 = 0

        # Expected x offsets for names/stats
        self.image_x_offsets = {
            "names": {"x2": 130, "x1": 320},
            "stats": {"x2": 320, "x1": None} } # x1 set in parse_scoreboard

        #***OCR Configs***
        # OEM 1: NN LSTM ENGINE
        self.oem = 1
        # PSM 6: Single uniform block of text
        self.psm = 6

        # Stat Whitelist
        self.stat_whitelist = "0123456789"
        # name Whitelist
        self.name_whitelist = "qwertyuiopasdfghjklzxcvbnm"  \
            + "QWERTYUIOPASDFGHJKLZXCVBNM.-"

    ##########################
    # PUBLIC MEMBER FUNCTIONS
    ##########################

    def parse_scoreboard(self, filename: str) -> None:
        """
            Params:
               filame (str): source filename
        """

        cv2_image = cv2.imread(os.path.join(
            self.image_dir,
            filename))

        x = int(cv2_image.shape[1])
        self.image_x_offsets['stats']['x1'] = x

        # Expect at most 10 rows in an image
        for i in range(1, 10):
            # Create pre-process image names
            names_source_image = os.path.join(
                self.results_dir,
                f"subimg_names_{self.player_count}.png")

            names_final_image = os.path.join(
                self.results_dir,
                f"binary_names_{self.player_count}.png")

            stats_source_image = os.path.join(
                self.results_dir,
                f"subimg_stats_{self.player_count}.png")

            stats_final_image = os.path.join(
                self.results_dir,
                f"binary_stats_{self.player_count}.png")

            # Create Image with correct offset: names
            # TODO: Below 80 lines
            try:
                cv2.imwrite(
                    names_source_image, # CV2 Image
                    cv2_image[
                        self.y2:self.y1,
                        self.image_x_offsets["names"]['x2']:self.image_x_offsets["names"]['x1'], :]
                )
            except Exception as e:
                self._reset_y1_y2()
                break

            # Create Image with correct offset: stats
            # TODO: Below 80 lines
            try:
                cv2.imwrite(
                    stats_source_image, # CV2 Image
                    cv2_image[
                        self.y2:self.y1,
                        self.image_x_offsets["stats"]['x2']:self.image_x_offsets["stats"]['x1'], :]
                )
            except Exception as e:
                self._reset_y1_y2()
                break

            # Name/stats parsing
            self._parse_names(cv2_image, names_source_image, names_final_image)
            self._parse_stats(cv2_image, stats_source_image, stats_final_image)

            # LOOP END SHIFT ROW DOWN
            self.y2 = self.y1 + (4*i)
            self.y1 = 55*(i+1) + (3*i)
            self.player_count  += 1
        # Reset Loop Variables
        self._reset_y1_y2()

    ##########################
    # PRIVATE MEMBER FUNCTIONS
    ##########################

    def _reset_y1_y2(self):
        """Reset row heights
        """
        self.y1 = 55
        self.y2 = 0

    def _parse_names(self,
        cv2_image   : str,
        source_image: str,
        final_image : str) -> None:
        """Create overall_data keys equal to the players name
            Params:
                cv2_image    (str): cv2 image object
                source_image (str): Path to source
                final_image  (str): Path to final
        """
        # Create PreProcess object
        ImagePreProc = ImagePreProcessing()

        bin_image = ImagePreProc.img_to_binary(
            source_image, # Original sub image
            ImagePreProc.sharp_kernel, # Kernels
            final_image) # Binary output

        # Save binary output
        cv2.imwrite(final_image, bin_image)

        # load final bin_image
        final_bin_img = cv2.imread(final_image)

        # Start OCR - Configs
        config = f"--oem {self.oem} --psm {self.psm}" \
            + f" -c tessedit_char_whitelist={self.name_whitelist}"

        # OCR Results
        parsed_text = tess.pytesseract.image_to_string(final_bin_img,
            config = config)

        # Raw stat data
        data  = tess.pytesseract.image_to_data(final_bin_img,
            config = config, output_type=Output.DICT)

        parsed_text = parsed_text.split()
        parsed_name = ""
        for txt in parsed_text:
            if txt.isdigit() or txt == "|":
                continue
            parsed_name += txt

        self.last_name_key = parsed_name
        self.overall_data[self.last_name_key] = {}

    def _parse_stats(self,
        cv2_image   : str,
        source_image: str,
        final_image : str) -> None:
        """Create overall_data keys equal to the players name
            Params:
                cv2_image    (str): cv2 image object
                source_image (str): Path to source
                final_image  (str): Path to final
        """

        # Determine dominate color for the player
        average_color_row = np.average(cv2_image, axis = 0)
        average_color     = np.average(average_color_row, axis=0)


        # Create PreProcess object
        ImagePreProc = ImagePreProcessing()

        bin_image = ImagePreProc.img_to_binary(
            source_image, # Original sub image
            ImagePreProc.sharp_kernel, # Kernels
            final_image) # Binary output

        # Save binary output
        cv2.imwrite(final_image, bin_image)

        # load final bin_image
        final_bin_img = cv2.imread(final_image)

        # Start OCR
        config = f"--oem {self.oem} --psm {self.psm}" \
            + f" -c tessedit_char_whitelist={self.stat_whitelist}"

        parsed_data  = tess.pytesseract.image_to_data(final_bin_img,
            config = config, output_type=Output.DICT)

        '''Variables to keep track of parsed data results
            - Box area/cords are used tolLoop through images
                with low confidence and create
                new sub images

        '''

        true_stats  = []
        true_confs  = []
        top_cords   = []
        left_cords  = []
        widths      = []
        heights     = []

        mean_box_area    = 0
        box_count        = 0
        std_box_area     = 0
        box_area_list    = []

        # for each parsed data result, check if it is a digit
        # We only expected digits everything right of names
        for i in range(0, len(parsed_data['text'])):
            if not parsed_data['text'][i].isdigit():
                continue

            # Stats: <score ... healing done>
            true_stats.append(int(parsed_data['text'][i]))
            true_confs.append(float(parsed_data['conf'][i]))

            # Box info
            top_cords.append(parsed_data['top'][i])
            left_cords.append(parsed_data['left'][i])
            widths.append(parsed_data['width'][i])
            heights.append(parsed_data['height'][i])

        # Check if 6 boxes were found
        # Score, kills, deaths, assist, heals, and damage
        self.overall_data[self.last_name_key]['box_error'] = False
        if len(true_stats) < 6:
            self.overall_data[self.last_name_key]['box_error'] = True

        # Sum up rgb values
        rgb_sum = sum([round(rgb) for rgb in average_color])
        if rgb_sum not in self.rgb_sum_groups:
            self.rgb_sum_groups.append(rgb_sum)

        self.overall_data[self.last_name_key]['old_data'] = parsed_data

        self.overall_data[self.last_name_key]['text']   = true_stats
        self.overall_data[self.last_name_key]['conf']   = true_confs
        self.overall_data[self.last_name_key]['img']    = final_image
        self.overall_data[self.last_name_key]['left']   = left_cords
        self.overall_data[self.last_name_key]['top']    = top_cords
        self.overall_data[self.last_name_key]['width']  = widths
        self.overall_data[self.last_name_key]['height'] = heights
        self.overall_data[self.last_name_key]['team']   = rgb_sum

    def _ocr_conf_loop(self,
        source_image: str,
        final_image : str) -> list[list]:
        """Attempt to loop through multiple pre-processing settings
        untill a stable confidence is hit. Request user feedback if
        unable to get confidence > 60.

        Params:
            source_image (str): abs path to image
            final_image  (str): abs path to final image

        Returns:
            [true_stats, true_confs] (List): List of new parsed results.

        """
        # TODO: Loop through many configs to get the best possible results

        # Create PreProcess object
        ImagePreProc = ImagePreProcessing()

        # Start OCR
        config = f"--oem {self.oem} --psm 13" \
            + f" -c tessedit_char_whitelist={self.stat_whitelist}"

        parsed_data  = tess.pytesseract.image_to_data(
            cv2.imread(source_image),
            config = config,
            output_type=Output.DICT)

        true_stats = []
        true_confs = []
        for i in range(0, len(parsed_data['text'])):
            if not parsed_data['text'][i].isdigit():
                continue

            true_stats.append(int(parsed_data['text'][i]))
            true_confs.append(float(parsed_data['conf'][i]))

        return [true_stats, true_confs]

    def _split_box_err(self, img: cv2, data:dict) -> list:
        """
        """
        height, width, _ = img.shape
        x_offset = int(width/6.4)

        new_values = []
        new_confs  = []
        new_height = []
        new_width  = []
        new_top    = []
        new_left   = []

        x1 = 0
        x2 = x_offset
        for i in range(0,6):
            tmp_player_image = self.results_dir + f"_errbox_{i}.png"
            cv2.imwrite(
                tmp_player_image,
                img[
                    0:height,
                    x1:x2, :])

            new_left_value = x1+10 if x1 == 0 else x1
            new_left.append(new_left_value)
            x1 = x2
            x2 += x_offset

            new_sub_image = cv2.imread(tmp_player_image)

            # Start OCR - Configs
            config = f"--oem {self.oem} --psm {self.psm}" \
                + f" -c tessedit_char_whitelist={self.stat_whitelist}"

            # Raw stat data
            parsed_data = tess.pytesseract.image_to_data(new_sub_image,
                config = config, output_type=Output.DICT)

            new_values.append(parsed_data['text'][-1])
            new_confs.append(round(float(parsed_data['conf'][-1])))
            new_height.append(parsed_data['height'][-1])
            new_width.append(x_offset)
            new_top.append(parsed_data['top'][-1])

        data['text']   = new_values
        data['conf']   = new_confs
        data['height'] = new_height
        data['width']  = new_width
        data['top']    = new_top
        data['left']   = new_left

    def _review_results(self) -> None:
        """If any confidence values are under 75%, redo with new kernels

            Show images that still have low confidence after corrections
            Ask the user to enter in correct values
        """

        for name in self.overall_data:
            data             = self.overall_data[name]
            source_ocr_image = cv2.imread(data['img'])
            if len(data['conf']) != 6:
                # Attempt to fix heal/dmg grouping error
                print("ERROR BBOX FOR", name)
                self._split_box_err(source_ocr_image, data)

            for i in range(0, len(data['conf'])):
                # Attempt to create a new tighter bounding box around
                # the low confidence area
                if float(data['conf'][i]) < 75:

                    (x, y) = (data['left'][i] - 5, data['top'][i] - 5)
                    (w, h) = (data['width'][i] + 10, data['height'][i] + 10)


                    error_img_output = data['img'].replace(".png", f"_error_{i}.png")
                    final_img_output = data['img'].replace(".png", f"_error_{i}.png")

                    cv2.imwrite(error_img_output, source_ocr_image[y:y + h, x:x + w,:])

                    new_ocr_image = cv2.imread(error_img_output)

                    new_vals, new_confs = self._ocr_conf_loop(
                        error_img_output, final_img_output)

                    new_vals  += [data['text'][i]]
                    new_confs += [data['conf'][i]]

                    # Take the max confidence value
                    max_conf_index = new_confs.index(max(new_confs))

                    # Update text
                    self.overall_data[name]['text'][i] = new_vals[max_conf_index]
                    self.overall_data[name]['conf'][i] = new_confs[max_conf_index]

                    # Keep track of error values
                    self.overall_data[name]['err'] = [new_vals, new_confs]

                    conf_value = self.overall_data[name]['conf'][i]

                    # If confdience is still low, show image and ask for input
                    if int(conf_value) < 75:

                        if int(conf_value) > 60 and str(self.overall_data[name]['text'][i]) == "0":
                            continue # 0s are correct, High conf for them is ~>60

                        print("ELEMENT COUNT:", len(data['text']))
                        print(f"Low confidence {data['conf'][i]}. Guessed value = {data['text'][i]}")
                        cv2.rectangle(source_ocr_image, (x, y), (x + w , y + h), (0, 255, 0), 2)
                        while(True):
                            cv2.imshow('Error Image: Escape to skip or enter to save changes', source_ocr_image)
                            if cv2.waitKey(1)==27:
                                break
                            elif cv2.waitKey(1) == 13:
                                text = input("Correct score box values:")
                                # User input is treated as 100% conf
                                end_loop = True
                                while(end_loop):
                                    for answer in text.split(","):
                                        if not answer.isdigit():
                                            print("Invalid character found, please input values again.")
                                            text = input("Correct score box values: ")
                                            end_loop = True
                                        else:
                                            end_loop = False

                                # Human confs = 100%
                                new_answers = text.split(",")
                                new_confs = [100.00 for j in range(0,len(new_answers))]

                                # Add new answers/cnfs
                                data['conf'][i:i] = new_confs
                                data['text'][i:i] = new_answers

                                # Remove old answer
                                del data['conf'][i+len(new_confs)]
                                del data['text'][i+len(new_answers)]
                                break
                        cv2.destroyAllWindows()

            if data['box_error'] == True:
                self.overall_data[name]['text'].append(0)
                self.overall_data[name]['conf'].append(0)

    def _naive_simularity(self, a:str, b:str) -> int:
        """
        """

        score = 0
        for i in range(0, len(b)):
            if i > len(a)-1:
                return score
            if a[i] == b[i]:
                score += 1

        return score

    def _group_name_alignment(self, group_numbers:dict, name:str) -> str:
        """Use _naive_simularity to determine what key should be used.
        If no valid key, then group number will be empty
        """

        min_distance_key = ""
        max_simularity = float(-inf)

        for key in group_numbers:
            min_val = self._naive_simularity(key, name)
            if min_val > max_simularity:
                max_simularity = min_val
                min_distance_key = key

        if max_simularity < len(name)/2:
            return "err"
        return min_distance_key

    def _team_grouping(self):
        """Group by RGB values. +- 15

            Expected color variants of yellow, green, and purple
        """
        self.team_data = {
            "team_1": {"rgb_sum": min(self.rgb_sum_groups)},
            "team_2": {"rgb_sum": max(self.rgb_sum_groups)}
        }

        for name in self.overall_data:
            if name == "Empath":
                print("EMPATH", self.overall_data[name])
                text = input("WAIT")
            rgb_sum = self.overall_data[name]['team']
            if abs(rgb_sum - self.team_data['team_1']['rgb_sum']) <= 15:
                # Team one
                self.team_data['team_1'][name] = self.overall_data[name]['text']
            else:
                self.team_data['team_2'][name] = self.overall_data[name]['text']


    def _translate_overall_data(self, group_numbers:dict ) -> None:
        """Store parsed data for CSV parsing/building
        """
        self._team_grouping()

        for team in self.team_data:
            for name in self.team_data[team]:
                if name == "rgb_sum":
                    continue
                score_text = self.team_data[team][name]

                self.team_data[team][name] = {
                    "name"  : name,
                    "score" : int(score_text[0]),
                    "kills" : int(score_text[1]),
                    "deaths": int(score_text[2]),
                    "assist": int(score_text[3]),
                    "heals" : 0 if len(score_text) < 6 else int(score_text[4]),
                    "damage": 0 if len(score_text) < 6 else int(score_text[5]),
                    "group" : group_numbers[self._group_name_alignment(group_numbers, name)],
                    "team"  : team
                }



