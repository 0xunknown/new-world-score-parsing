"""
Copyright 2022 0xunknown <djbey@protonmail.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import os
import sys

from ScoreParsing import ScoreParsing
from WarRosterParsing import WarRosterParsing

from csv_output import *

def main():
    if(len(sys.argv) != 2):
        print("Invalid or missing arggument. Expected directory name")
        return

    names_to_group_map = {}
    NwParser = ScoreParsing(sys.argv[1])

    # For each image, parse and store the results
    for filename in NwParser.image_list:
        if "png" not in filename:
            continue
        elif "war_roster" in filename:
            rosterParser = WarRosterParsing(filename, sys.argv[1])
            rosterParser._parse_groups()
            names_to_group_map.update(rosterParser.group_numbers)
        else:
            NwParser.parse_scoreboard(filename)


    # Review Results -> Re-parse conf < 75% or unexpected output
    NwParser._review_results()
    NwParser._translate_overall_data(names_to_group_map)

    # Calculate metadata
    # NwParser.metadata()

    # Output to CSV
    save_output_to_csv(NwParser.results_dir, NwParser.team_data)



if __name__ == "__main__":
    main()