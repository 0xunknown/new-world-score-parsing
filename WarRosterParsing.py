"""
Copyright 2022 0xunknown <djbey@protonmail.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
from cmath import inf
import os
import cv2
from PIL import Image
import pytesseract as tess
import numpy as np
from pytesseract import Output

from ImagePreProcessing import ImagePreProcessing

class WarRosterParsing():
    def __init__(self,
    war_roster_img_name: str,
    data_dir:str) -> None:
        self.war_roster_img = os.path.join(
            "warimages",
            data_dir,
            war_roster_img_name)

        self.group_images = os.path.join(
            "warimages",
            data_dir,
            "groups.png")

        #***OCR Configs***
        # OEM 1: NN LSTM ENGINE
        self.oem = 1
        # PSM 6: Single uniform block of text
        self.psm = 11

        self.name_whitelist = "qwertyuiopasdfghjklzxcvbnm"  \
            + "QWERTYUIOPASDFGHJKLZXCVBNM.-"

        # Results
        self.group_numbers = {"err": ""}

    def parse_warboard(self) -> None:
        """
        """
        cv2_image = cv2.imread(self.war_roster_img)

        ImagePreProc = ImagePreProcessing()

    def _parse_groups(self) -> None:
        """
        """
        ImagePreProc = ImagePreProcessing()

        final_image = self.war_roster_img + "_bin.png"

        bin_image = ImagePreProc.img_to_binary(
            self.war_roster_img, # Original sub image
            ImagePreProc.sharp_kernel, # Kernels
            final_image) # Binary output

               # Save binary output
        cv2.imwrite(final_image, bin_image)

        # load final bin_image
        final_bin_img = cv2.imread(final_image)

        # Full image width
        full_image_width = int(final_bin_img.shape[0])
        full_image_height = int(final_bin_img.shape[1]) #0.91/2

        # Start OCR - Configs
        config = f"--oem {self.oem} --psm {self.psm}" \
            + f" -c tessedit_char_whitelist={self.name_whitelist}"

        # Raw stat data
        parsed_data = tess.pytesseract.image_to_data(final_bin_img,
            config = config, output_type=Output.DICT)

        # Find cords for army location
        army_y0 = 0
        army_x0 = 0
        for i in range(0, len(parsed_data['text'])):
            if parsed_data['text'][i] == "ARMY":
                # Army bounding box
                army_y0 = int(parsed_data['top'][i] + parsed_data['height'][i])
                army_x0 = int(parsed_data['left'][i])
                break

        # Collect 10 the 10 groups
        # Expected Group height cords
        y1 = int(army_y0*4)
        y2 =  int((full_image_height - y1 - (0.45*full_image_height))/2)#int(full_image_height/2) #int(army_y0*10.3)

        x1 = int(army_x0)+1
        x_offset = int((full_image_width)/4) #int(army_x0 + army_x0*0.55)
        x2 = x1 + x_offset - 3

        x1_gap = 10 # TODO:
        x2_gap = 5  # TODO

        # Group images
        group_image_list = []
        for i in range(1,11):
                if i == 6:
                    y1 = y2 + int(army_y0*2.5)
                    y2 = int(y2*1.9)

                    # Reset x cords
                    x_offset = int((full_image_width)/4) #int(army_x0 + army_x0*0.55)
                    x1 = int(army_x0)
                    x2 = x1 + x_offset

                group_image = self.group_images + f"_group{i}.png"
                group_image_list.append(group_image)

                cv2.imwrite(
                    group_image,
                    final_bin_img[
                        y1:y2,
                        x1:x2, :]
                )

                x1 = x2 + x1_gap*(i)
                x2 += x_offset + x2_gap*(i)

                # TODO: Something is wrong with split logic..
                if i in [2,3,4,6,7,8,9]:
                    x1 -= x1_gap*(i-1)
                    x2 -= x2_gap*(i-1)

        # Split groups into single name images
        player = 1
        group_number = 0
        for group_img in group_image_list:
            group_bin_image = cv2.imread(group_img)
            image_height = int(group_bin_image.shape[1])
            image_width = int(group_bin_image.shape[0])

            y0 = 10 # TODO
            y_offset     = int(image_height/6.7)
            space_offset = int(y_offset*0.2)
            x1 = int(image_width*0.25)
            x2 = int(image_width*0.8) # TODO*
            if group_number >= 5:
                x1 -= x1_gap-1
            for j in range(1,6):
                tmp_player_image = group_image + f"_p{player}.png"
                cv2.imwrite(
                    tmp_player_image,
                    group_bin_image[
                        y0:y0+y_offset,
                        x1:x2, :]
                )

                y0     += y_offset + space_offset + 5
                player += 1

                 # Get parsed results
                group_final_image = cv2.imread(tmp_player_image)

                # Start OCR - Configs
                config = f"--oem {self.oem} --psm {self.psm}" \
                    + f" -c tessedit_char_whitelist={self.name_whitelist}"

                # Raw stat data
                parsed_data = tess.pytesseract.image_to_data(group_final_image,
                    config = config, output_type=Output.DICT)

                # Keep the largest string result
                name_index = 0
                max_count  = float(-inf)
                for k in range(0, len(parsed_data['text'])):
                    possible_name = parsed_data['text'][k]
                    if len(possible_name) > max_count:
                        max_count = len(possible_name)
                        name_index = k

                # Update name to group info
                # group_number started at index 0
                self.group_numbers[parsed_data['text'][name_index]] = group_number+1
            group_number += 1



