"""
Copyright 2022 0xunknown <djbey@protonmail.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
import os
import cv2
import numpy as np
from PIL import Image

class ImagePreProcessing():
    def __init__(self):
        # Kernels
        self.sharp_kernel = [
            [-1,-1,-1],
            [-1,9,-1],
            [-1,-1,-1]]
        self.identity_kernel = [
            [0,0,0],
            [0,1,0],
            [0,0,0]]
        self.mexican_hat_kernel = [
            [0,0,-1,0,0],
            [0,-1,-2,-1,0],
            [-1,-2,16,-2,-1],
            [0,-1,-2,-1,0],
            [0,0,-1,0,0]]

        # Translate to NP array
        self.sharp_kernel       = np.array(self.sharp_kernel)
        self.identity_kernel    = np.array(self.identity_kernel)
        self.mexican_hat_kernel = np.array(self.mexican_hat_kernel)

    def set_sharp_kernel(self, value: int) -> None:
        """Adjust the middle matrix value of the kernel
            Params:
                value (int): 1+
        """
        if not type(value, int):
            return

        self.sharp_kernel[1][1] = value

    def img_to_binary(
        self,
        img_source: str,
        kernel    : np.ndarray,
        img_dest  : str) -> "cvw.image":
        """Transformation of normal image for OCR
            Image -> grayscale -> sharpen/filter ->
            <morphology> -> binary/threshold

            Params:
                img_path (str): Absolute path to image
                kernel   (np.ndarray): Kernel Matrix
                img_dest (str): Absolute path to image dest

            Returns:
                binary_image (cv2.image): Final image to use for OCR
        """
        # Load Source
        source_img = cv2.imread(img_source)

        # Convert to grayscale
        grayscale_img = cv2.cvtColor(source_img, cv2.COLOR_BGR2GRAY)

        # Sharpen/Filter TODO: Differentiate
        sharpen_kernel = kernel
        sharpen_image  = cv2.filter2D(grayscale_img, -1, sharpen_kernel)

        # Binary Conversion w/ threshold
        _, binary_stat_image = cv2.threshold(
            sharpen_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        # Make text black & background white
        count_white = np.sum(binary_stat_image > 0)
        count_black = np.sum(binary_stat_image == 0)

        if count_black > count_white:
           binary_stat_image = 255 - binary_stat_image

        return binary_stat_image